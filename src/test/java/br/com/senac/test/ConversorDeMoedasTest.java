
package br.com.senac.test;

import br.com.senac.ex3.ConversorDeMoedas;
import org.junit.Test;
import static org.junit.Assert.*;


public class ConversorDeMoedasTest {
    
    public ConversorDeMoedasTest() {
    }
    
    
    @Test
    public void deveConverter10ReaisPara2E45DollarAmericano(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.converterParaDollarAmericano(real);
        assertEquals(2.45, resultado , 0.01);
    }
    
    @Test
    public void deveConverter10ReaisPara3E38DollarAustraliano(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.converterParaDollarAustraliano(real);
        assertEquals(3.38, resultado , 0.01);
        
    }
    
     @Test
    public void deveConverter10ReaisPara2E08Euros(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.converterParaEuro(real);
        assertEquals(2.08, resultado , 0.01);
        
    }
    
    
    
}
